import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'angular-ionic-nest-mongo',
  webDir: 'www'
};

export default config;
